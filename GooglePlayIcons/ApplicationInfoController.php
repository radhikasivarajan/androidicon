<?php
require './vendor/autoload.php';
include_once __DIR__ . '/src/DatabaseConnector.php';
include_once __DIR__ . '/src/DatabaseFunctions.php';

use Raulr\GooglePlayScraper\Scraper;

class ApplicationInfoController
{
    const BASE_URL = 'https://play.google.com/store/apps/details?id=';
    private $db;
    private $appGateway;

    // instantiate database and DatabaseFunctions object
    public function __construct()
    {
        $database = new DatabaseConnector();
        $this->db = $database->getConnection();
        $this->appGateway = new DatabaseFunctions($this->db);
    }

    // Method to get app info using Google play scrapper (App image url, version etc.)
    public function getAppDetailsByScraper($appId)
    {
        try {
            $scraper = new Scraper();
            $appDetails = $scraper->getApp($appId);

            $AppData = array(
                "app_id" => $appId,
                "app_img_url" => $appDetails['image'],
                "app_last_update" => date_format(
                    date_create($appDetails['last_updated']), "Y-m-d"
                ),
                "app_version" => $appDetails['version'],
                "app_title" => $appDetails['title'],
                "app_author" => $appDetails['author'],
                "app_author_link" => $appDetails['author_link'],
                "app_categories" => $appDetails['categories'][0],
                "app_size" => $appDetails['size'],
                "app_content_rating" => $appDetails['content_rating'],
            );
            return $AppData;
        } catch (exception $e) {}
    }

    // Get stored image url value of the given app from database
    public function getAppImageURL($appId)
    {
        $data = $this->appGateway->getImageURL($appId);
        return $data['image_url'];
    }

    // Get stored image of the given app from database (Type: blob)
    public function getAppImage($appId)
    {
        $data = $this->appGateway->getImageData($appId);
        return $data['image_data'];
    }

    // Method to update only image URL of specific app to Database
    public function updateAppImageURL($appId)
    {
        $appData = $this->getAppDetailsByScraper($appId);
        // If app  available at the playstore
        if ($appData) {
            $numRows = $this->appGateway->updateImageURL($appId, $appData["app_img_url"]);
            return $numRows;
        }
        return false;
    }

    // Method to update only image of specific app to Database
    public function updateAppImage($appId)
    {
        $appData = $this->getAppDetailsByScraper($appId);
        // If app  available at the playstore
        if ($appData) {
            $imgData = file_get_contents($appData["app_img_url"]);
            if ($imgData === false) {
                throw new Exception('Failed to download file at: ' . $appData["app_img_url"]);
            }
            $numRows = $this->appGateway->updateImageBLOB($appId, $imgData);
            return $numRows;
        }
        return false;
    }

    // Method to save all app info (except blob image) of specific app to the Database.
    public function updateAppDetails($appId)
    {
        $appData = $this->getAppDetailsByScraper($appId);
        // If app  available at the playstore
        if ($appData) {
            $numRows = $this->appGateway->updateAppInfo($appId, $appData);
            return $numRows;
        }
        return false;
    }

    // Method to update all apps with it's info (except blob image).
    public function updateAllApps()
    {
        try {
            $numAppsUpdated = 0;
            $numAppsNotUpdated = 0;
            $allAppIds = $this->appGateway->getAllAppIds();
            foreach ($allAppIds as $appData) {
                foreach ($appData as $app) {
                    $status = $this->updateAppDetails($app);
                    if ($status == 1) {
                        $numAppsUpdated += $numAppsUpdated;
                    } else {
                        $numAppsNotUpdated += $numAppsNotUpdated;
                    }
                }
            }
            return array(
                "apps_updated" => $numAppsUpdated,
                "apps_not_updated" => $numAppsNotUpdated,
            );
        } catch (exception $e) {
            echo "</br>Error in  updating all app</br>";
            echo $e->getMessage();
        }
    }

    // Method to update apps with it's info (except blob image) which doesn't have image URL in the Database.
    public function updateAppsWithEmptyImage()
    {
        try {
            $numAppsUpdated = 0;
            $numAppsNotUpdated = 0;
            $allAppIds = $this->appGateway->getEmptyImageAppIds();
            foreach ($allAppIds as $appData) {
                foreach ($appData as $app) {
                    $status = $this->updateAppDetails($app);
                    if ($status == 1) {
                        $numAppsUpdated++;
                    } else {
                        $numAppsNotUpdated++;
                    }
                }
            }
            return array(
                "apps_updated" => $numAppsUpdated,
                "apps_not_updated" => $numAppsNotUpdated,
            );
        } catch (exception $e) {
            echo "</br>Error in updating all app without image</br>";
            echo $e->getMessage();
        }
    }

    // Method to save image locally in the folder 'images'
    public function saveAppImageLocal($url, $appId = null)
    {
        $fileName = "logo";
        if ($appId != null) {
            $fileName = str_replace(".", "_", $appId);
        }
        if (!is_dir(__DIR__ . '/images/')) {
            mkdir(__DIR__ . '/images/');
        }
        if ($url) {
            $fileContents = file_get_contents($url);
            if ($fileContents === false) {
                throw new Exception('Failed to download file at: ' . $url);
            }
            $filePath = __DIR__ . '/images/' . $fileName . '.png';
            file_put_contents($filePath, $fileContents);
            return $filePath;
        }
        return "Image not available to save.";
    }
}
