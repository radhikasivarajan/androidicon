<?php
class DatabaseFunctions
{

    // database connection and table name
    private $conn;
    private $table_name = "application_details";

    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getImageURL($appId)
    {
        $query = "SELECT DISTINCT application_image_url
        FROM  $this->table_name
        WHERE application_package_name = :application_package_name";
        try {
            $statement = $this->conn->prepare($query);
            $statement->execute(array(
                'application_package_name' => $appId,
            ));
            $resultURL = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if ($resultURL) {
                return array("image_url" => $resultURL[0]['application_image_url']);
            }
            return false;
        } catch (\PDOException $e) {
            echo "Error in getting image url</br>";
            exit($e->getMessage());
        }
    }

    public function getImageData($appId)
    {
        $query = "SELECT application_image_blob
        FROM  $this->table_name
        WHERE application_package_name = :application_package_name
        AND application_image_blob IS NOT NULL";
        try {
            $statement = $this->conn->prepare($query);
            $statement->execute(array(
                'application_package_name' => $appId,
            ));
            $statement->bindColumn(1, $image, PDO::PARAM_LOB);
            $result = $statement->fetch(PDO::FETCH_BOUND);
            if ($result) {
                return array("image_data" => $image);
            }
            return false;
        } catch (\PDOException $e) {
            echo "Error in getting image data</br>";
            exit($e->getMessage());
        }
    }

    public function getAllAppIds()
    {
        $query = "SELECT DISTINCT application_package_name
        FROM  $this->table_name
        ORDER BY application_package_name ASC";
        try {
            $statement = $this->conn->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            echo "Error in getting all app ids</br>";
            exit($e->getMessage());
        }
    }

    public function getEmptyImageAppIds()
    {
        $query = "SELECT DISTINCT application_package_name
        FROM $this->table_name
        WHERE application_image_url IS NULL";
        try {
            $statement = $this->conn->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            echo "Error in getting all app ids with empty img url</br>";
            exit($e->getMessage());
        }
    }

    public function updateImageURL($appId, $imageURL)
    {
        $query = "UPDATE $this->table_name
        SET application_image_url = :application_image
        WHERE application_package_name = :application_package_name";
        try {
            $statement = $this->conn->prepare($query);
            $statement->execute(array(
                'application_package_name' => $appId,
                'application_image' => $imageURL,
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            echo "Error in updating image url to DB</br>";
            exit($e->getMessage());
        }
    }

    public function updateImageBLOB($appId, $imageData)
    {
        $query = "UPDATE $this->table_name
        SET application_image_blob = :application_image_data
        WHERE application_package_name = :application_package_name";
        try {
            $statement = $this->conn->prepare($query);
            $statement->execute(array(
                'application_package_name' => $appId,
                'application_image_data' => $imageData,
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            echo "Error in updating image to DB</br>";
            exit($e->getMessage());
        }
    }

    public function updateAppInfo($appId, $appData)
    {
        $query = "UPDATE $this->table_name
        SET application_image_url = :application_image_url,
            application_title = :application_title,
            application_author = :application_author,
            application_author_link = :application_author_link,
            application_version = :application_version,
            application_last_updated = :application_last_updated,
            application_size = :application_size,
            application_category = :application_category,
            application_content_rating = :application_content_rating
        WHERE application_package_name = :application_package_name";
        try {
            $statement = $this->conn->prepare($query);
            $statement->execute(array(
                'application_package_name' => $appId,
                'application_image_url' => $appData['app_img_url'],
                'application_title' => $appData['app_title'],
                'application_author' => $appData['app_author'],
                'application_author_link' => $appData['app_author_link'],
                'application_version' => $appData['app_version'],
                'application_last_updated' => $appData['app_last_update'],
                'application_size' => $appData['app_size'],
                'application_category' => $appData['app_categories'],
                'application_content_rating' => $appData['app_content_rating'],
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            echo "Error in updating image details to DB</br>";
            exit($e->getMessage());
        }
    }
}
