# Play store app icons

## Table of Contents

- [Website](#website)
- [To Install on local machine](#to-install-on-local-machine)
- [Technologies used](#technologies-used)
- [Dependencies](#dependencies)

## Website

[https://www.tabnova.com/](https://www.tabnova.com/)

## To Install on local machine

- Clone this repo to the root directory of web server (www in WAMP, htdocs in MAMP and XAMPP).
- Start web server(WAMP, MAMP or XAMPP)
- In the terminal navigate to the folder where the repository.
- Run the command `composer install` to download the required dependencies inside the project.
- In the browser goto the URL `localhost:8888/androidicon/`.
- To run test type `./vendor/bin/phpunit tests` in the project directory.

## Technologies used

- PHP
- Composer

## Dependencies

### Packages and libraries

1. `google-play-scraper` (A PHP scraper to get app data from Google Play.)
