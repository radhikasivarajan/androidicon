<?php

// ************** EXAMPLES ************** //

include_once './GooglePlayIcons/ApplicationInfoController.php';

$app = new ApplicationInfoController();
$appId = "com.facebook.orca";

// GET METHODS (SPECIFIC APP)
$appDetails = $app->getAppDetailsByScraper($appId); // App details from google scrapper
$imageURLFromDB = $app->getAppImageURL($appId); // Image URL from DB storage
$imageFromDB = $app->getAppImage($appId); // Image from DB storage

// UPDATE METHODS (SPECIFIC APP)
$numURL = $app->updateAppImageURL($appId); // Update image URL to DB
$numImage = $app->updateAppImage($appId); // Update image to DB
$numApp = $app->updateAppDetails($appId); // Update app details to DB

// UPDATE METHODS (BULK)
// Update all apps with the info
// $numAllApp = $app->updateAllApps(); 

// Update all apps with empty image url in the DB
// $numEmptyImage = $app->updateAppsWithEmptyImage(); 

// SAVE IMAGE IN DIRECTORY
$saveStatus = $app->saveAppImageLocal($appDetails["app_img_url"]);


// DISPLAY RESULTS
echo "</br><b>AppScraper url: </b>" . $appDetails["app_img_url"] . "</br>";
echo "</br><img src=" . $appDetails["app_img_url"] . ">";

echo "</br><b>AppURLFromDB: </b>" . $imageURLFromDB . "</br>";

echo "</br><b>" . $numURL . "</b> row updated (URL) to DB</br>";
echo "</br><b>" . $numImage . "</b> row updated (IMG) to DB</br>";
echo "</br><b>" . $numApp . "</b> row updated (App details) to DB</br>";

echo "</br>Image saved successfully locally in <b>$saveStatus</b></br>";


// DISPLAY IMAGE FROM BLOB DATA
// header("Content-Type: image");
// echo $imageFromDB;

// DISPLAY BULK UPDATE STATUS
// var_dump($numAllApp);
// var_dump($numEmptyImage);

